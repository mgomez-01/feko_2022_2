#!/bin/bash

export DEBIAN_FRONTEND=noninteractive

sudo apt-get update
sudo apt-get install -y desktop-file-utils libcups2:amd64 libcurl4:amd64 \
    libfontenc1:amd64 libgcrypt20:amd64 libglu1-mesa:amd64 libgomp1:amd64 \
    libjpeg62:amd64 libltdl7:amd64 liblz4-1:amd64 libnss3:amd64 \
    libpam0g:amd64 libpci3:amd64 libtiff5:amd64 libxcomposite1:amd64 \
    libxcursor1:amd64 libxi6:amd64 libxkbcommon0:amd64 libxm4:amd64 \
    libxml2:amd64 libxrandr2:amd64 libxss1:amd64 libxtst6:amd64 lsb-core \
    net-tools shared-mime-info xkb-data xvfb

sudo apt-get install -y redsocks

sudo apt-get install -y qt5-default

sudo cp /local/repository/redsocks.conf /etc
