#!/bin/bash

sudo useradd fekouser
sudo apt update && sudo apt upgrade -y
sudo apt-get install screen

cd /opt/feko && sudo wget --output-document feko_2022.zip "https://drive.google.com/uc?export=download&id=1lfzdetmI50kesXuBqzsz-FazjblWHAT8&confirm=yes"

sudo unzip feko_2022.zip

sudo touch appending_to_profile.sh
sudo cat /etc/.profile | sudo grep altair
if [ $? -eq 1 ]; then
    sudo echo "

    # sourcing altair init
    source /opt/feko/2022/altair/feko/bin/initfeko

    ALTAIR_PATH=\"/opt/feko/2022/altair/feko/bin\"
    if [ \$( (echo \$PATH) | grep 2022/altair) ]; then
	    echo \"Altair already present already in path.\"
    else
	    export PATH=\$PATH:\$ALTAIR_PATH
    fi

    if [ \$( (echo \$ALTAIR_WINPROP) | grep winprop) ]; then
	    echo \"winprop dir already set up\"
    else
	    export ALTAIR_WINPROP=\"/opt/feko/2022/altair/feko/api/winprop/\"
    fi
    export ALTAIR_LICENSE_PATH='6200@winlic-d.eng.utah.edu'

    if ! echo \$LD_LIBRARY_PATH | grep -q '/opt/feko/2022/altair/feko/api/winprop/bin'; then
      export LD_LIBRARY_PATH=\"/opt/feko/2022/altair/feko/api/winprop/bin\":\$LD_LIBRARY_PATH;
      echo 'Path added to LD_LIBRARY_PATH.';
    else
      echo 'Path already in LD_LIBRARY_PATH. No action taken.';
    fi

    " | sudo tee -a  appending_to_profile.sh > /dev/null

    sudo chmod u+x appending_to_profile.sh
    sudo ./appending_to_profile.sh

    sudo sh -c 'cat appending_to_profile.sh >> /etc/.profile'

fi

echo "testing Winprop CLI"
source /etc/.profile

/opt/feko/2022/altair/feko/bin/WinPropCLI --help

if [ $? -eq 0 ]; then
    echo -e "WinPropCLI setup successful!!"
else
    echo -e "WinPropCLI setup failure :/"
fi


