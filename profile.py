"""
Profile for CST.

Connect to the compute node, add `-L 5900:localhost:5900` to the ssh command as
well as `-A` if you want ssh-agent forwarding (to make the connection back
out). On the node run:

    /local/repository/license_setup.sh

This will step you through the process of ssh'ing to a computer with access to
the CADE license servers. Once done it will print "All done". Then you can
connect with VNC to localhost:5900 on your local machine.

If you wish to run multiple of these change the first number in the -L command
to a new port and connection to that new port when you VNC. For instance use
`-L 5901:localhost:5900` and connect with VNC to localhost:5901.

CST is in the applications menu with its own group.
"""

import profiles.profile_base

DATASET = "urn:publicid:IDN+emulab.net:antennas+stdataset+feko2022_testing"

profiles.profile_base.make_profile(
    "urn:publicid:IDN+emulab.net+image+PowderTeam:CST_2021_SP2_v2",
    DATASET,
    True
)
