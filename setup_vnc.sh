#!/bin/bash

export DEBIAN_FRONTEND=noninteractive

sudo apt-get install -y iptables-persistent
sudo systemctl enable netfilter-persistent.service

sudo cp /local/repository/rules.v4 /etc/iptables/rules.v4
sudo cp /local/repository/rules.v6 /etc/iptables/rules.v6

sudo iptables-restore < /etc/iptables/rules.v4
sudo ip6tables-restore < /etc/iptables/rules.v6


# Temporary until next disk image gets built:
sudo apt-get update
sudo apt-get install -y debconf-utils
echo "gdm3 shared/default-x-display-manager select lightdm" | sudo debconf-set-selections
echo "lightdm shared/default-x-display-manager select lightdm" | sudo debconf-set-selections

export DEBIAN_FRONTEND=noninteractive
sudo apt-get install -y x11vnc xfce4 xfce4-terminal

sudo systemctl disable lightdm || /bin/true
sudo systemctl stop lightdm || /bin/true
sudo systemctl disable gdm || /bin/true
sudo systemctl stop gdm || /bin/true
