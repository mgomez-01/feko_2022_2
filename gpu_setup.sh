#!/bin/bash -e

export DEBIAN_FRONTEND=noninteractive

export DISTRO=ubuntu2004
export ARCHITECTURE=x86_64
sudo apt update
sudo apt upgrade -y apt
sudo add-apt-repository -y ppa:graphics-drivers
sudo apt-key adv --fetch-keys https://developer.download.nvidia.com/compute/cuda/repos/${DISTRO}/${ARCHITECTURE}/7fa2af80.pub
sudo bash -c "echo deb\ http://developer.download.nvidia.com/compute/cuda/repos/${DISTRO}/${ARCHITECTURE}/\ / > /etc/apt/sources.list.d/cuda.list"
sudo bash -c "echo deb\ http://developer.download.nvidia.com/compute/machine-learning/repos/${DISTRO}/${ARCHITECTURE}\ / > /etc/apt/sources.list.d/cuda_learn.list"

sudo apt-get update
# Needs to be <=470 to work with Tesla K80
sudo apt-get install -y nvidia-driver-470 libnvidia-gl-470 libnvidia-common-470 nvidia-dkms-470 nvidia-kernel-source-470 nvidia-compute-utils-470 nvidia-utils-470 xserver-xorg-video-nvidia-470 libnvidia-cfg1-470 libnvidia-compute-470 libnvidia-decode-470 libnvidia-encode-470 libnvidia-ifr1-470 libnvidia-fbc1-470 libnvidia-gl-470 nvidia-kernel-common-470
# This pacakge signals cuda-runtime what version to use (and prevents
# uninstalling 470 and switching to 510)
sudo apt-get install -y cuda-drivers-470 cuda-drivers=470*

for pkg in nvidia-driver-470 libnvidia-gl-470 libnvidia-common-470 nvidia-dkms-470 nvidia-kernel-source-470 nvidia-compute-utils-470 nvidia-utils-470 xserver-xorg-video-nvidia-470 libnvidia-cfg1-470 libnvidia-compute-470 libnvidia-decode-470 libnvidia-encode-470 libnvidia-ifr1-470 libnvidia-fbc1-470 libnvidia-gl-470 nvidia-kernel-common-470 cuda-drivers-470
do
    echo "${pkg} hold" | sudo dpkg --set-selections
done

sudo apt-get install -y cuda-11-4
sudo modprobe nvidia
sleep 5
nvidia-smi

