To install, create a dataset at least 32 GiB.
Create a profile called cst_image_create (special name) pointing at this git
    repo. 
Fire up an instance with dataset urn from above.

* Make connection to license server
    - Run /local/repository/license_setup.sh
    - SSH to something with -D 8080 to get access to license server.
* Install CST
    - Untar CST_S2_2022.CST_S2_2022.SIMULIA_CST_Studio_Suite.Linux64.tar
    - Run sudo SIMULIA_CST_Studio_Suite.Linux64/install.sh from within VNC
    - Install Set: Just check "Graphical frontend and command line interface"
    - Select "Flexnet-based licensing"
    - Select "Point to an existing CST license server system"
    - License server should be 27030@winlic-a.eng.utah.edu (not host:port)
    - Username "cstuser"
    - Install to something under /opt/cst, should be the default anyway.
 
