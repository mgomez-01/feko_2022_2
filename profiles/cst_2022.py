"""
Profile for CST 2022.

Connect to the compute node, if you want ssh-agent forwarding (to make the
connection back out) add `-A` to the ssh command. On the node run:

    /local/repository/license_setup.sh

This will step you through the process of ssh'ing to a computer with access to
the CADE license servers (like one of the CADE lab machines). Once done it will
print "All done".

CST is in the applications menu with its own group.
"""

import profiles.profile_base

DATASET = \
    "urn:publicid:IDN+emulab.net:powderteam+ltdataset+CST-application-2022"

profiles.profile_base.make_profile(
    "urn:publicid:IDN+emulab.net+image+PowderTeam:CST-image-2022",
    DATASET
)
