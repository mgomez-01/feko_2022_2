"""
Create image for CST Profile.
Mounts the dataset as read/write as opposed to the normal read-only.
"""

import profiles.profile_base

profiles.profile_base.make_profile(
    "urn:publicid:IDN+emulab.net+image+emulab-ops//UBUNTU20-64-STD",
    dataset=None,
    setup=True)
